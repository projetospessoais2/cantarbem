<?php 
    $utmSource = !empty($_GET["utm_source"]) ? $_GET["utm_source"] : null;
    $utmMedium = !empty($_GET["utm_medium"]) ? $_GET["utm_medium"] : null;
    $utmCampaign = !empty($_GET["utm_campaign"]) ? $_GET["utm_campaign"] : null;
    $utmTerm = !empty($_GET["utm_term"]) ? $_GET["utm_term"] : null;
    $utmContent = !empty($_GET["utm_content"]) ? $_GET["utm_content"] : null;
    $utmSite = !empty($_GET["utm_site"]) ? $_GET["utm_site"] : null;
?>

<div id="background">
    <div class="row mt-5 marginScreen" id="rowGeral">
        <div class="col-lg-1 col-xxl-2"></div>
        <div class="col-lg-6 col-xxl-4 text-center marginMobile">
            <img src="./assets/logo.webp" alt="" class="mt-2 card-img logo">
            <div class="d-none d-md-block mt-5"></div>
            <div class="d-md-none mt-3"></div>
            <div class="font-weight-bolder textLarge">
                Uma aula que vai <span class="text-red">transformar a sua voz e sua forma de cantar,</span> mesmo começando do zero
            </div>

            <div class="row mt-4">
                <div class="col-1 col-md-2"></div>
                <div class="col-3 col-md-2 d-flex align-items-center justify-content-center">
                    <img src="./assets/calendar.webp" alt="" class="card-img calendar">
                </div>
                <div class="col-6 col-md-6 d-flex align-items-center justify-content-center flex-column font-weight-bolder">
                    <span class="textBigger">
                        Terça-feira
                    </span>
                    <div class="text-red textLarge">
                        às 20h ao vivo
                    </div>
                </div>
                <div class="col-2 col-md-2"></div>
            </div>

            <div class="mt-3">
                <form method="POST" action="https://cantarbem.activehosted.com/proc.php" id="_form_6_" class="_form _form_6 _inline-form  _dark" novalidate data-styles-version="5">
                    <input type="hidden" name="u" value="6" />
                    <input type="hidden" name="f" value="6" />
                    <input type="hidden" name="s" />
                    <input type="hidden" name="c" value="0" />
                    <input type="hidden" name="m" value="0" />
                    <input type="hidden" name="act" value="sub" />
                    <input type="hidden" name="v" value="2" />
                    <input type="hidden" name="or" value="6ab55706b764756a02773b426c7b9993" />

                    <input type="text" name="email" id="email" class="form-control input" placeholder="Email">

                    <!-- UTMS -->
                    <input type="hidden" id="field[27]" name="field[27]" value="<?= $utmSource ?>" placeholder=""/>
                    <input type="hidden" id="field[28]" name="field[28]" value="<?= $utmContent ?>" placeholder=""/>
                    <input type="hidden" id="field[29]" name="field[29]" value="<?= $utmMedium ?>" placeholder=""/>
                    <input type="hidden" id="field[30]" name="field[30]" value="<?= $utmCampaign ?>" placeholder=""/>
                    <input type="hidden" id="field[31]" name="field[31]" value="<?= $utmTerm ?>" placeholder=""/>
                    <input type="hidden" id="field[32]" name="field[32]" value="<?= $utmSite ?>" placeholder=""/>

                    <!-- DATA -->
                    <!-- <input type="hidden" id="field[15]" name="field[15]" value="<?php //$todaysDate ?>" placeholder=""/> -->
                    
                    <button id="_form_6_submit" class="btn btn-participar mt-3">QUERO PARTICIPAR GRATUITAMENTE!</button>
                </form>
            </div>
        </div>
        <div class="col-lg-5 col-xxl-6"></div>
    </div>
    <br><br>
    <!-- <br><br><br><br><br>
    <br><br><br><br><br> -->
</div>