<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <link rel="icon" type="image/x-icon" href="assets/favicon.webp">
        <title>Super Aula Cantar Bem [ SA ] – Página B – Cantar Bem – Curso de Canto Completo</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/used.css">

        <!-- <link rel="stylesheet" href="css/fonts.css">
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <link rel="stylesheet" href="css/responsividade.css"> -->
    </head>

    <body>

        <?php require('./content.php'); ?>

        <script src="js/ac.js"></script>

    </body>

</html>